package kz.test.integrationapp;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MessageApi {
    @GET("users")
    Call<List<User>> getUsers();
}
