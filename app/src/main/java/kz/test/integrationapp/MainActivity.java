package kz.test.integrationapp;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static okhttp3.Request.Builder;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void facebook(View view) {
        String facebookPackageName = "com.facebook.katana";
        String facebookClassName = "com.facebook.katana.LoginActivity";

        try {
            getPackageManager().getApplicationInfo(facebookPackageName, 0);
            Intent intent = new Intent("android.intent.category.LAUNCHER");
            intent.setClassName(facebookPackageName, facebookClassName);
            startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(MainActivity.this, "Facebook не найден", Toast.LENGTH_LONG).show();
            Uri uri = Uri.parse("market://details?id=" + facebookPackageName);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
    }

    public void twitter(View view) {
        EditText editText = findViewById(R.id.twitterEditText);
        String searchText = editText.getText().toString();
        if (searchText.length() > 0) {
            try {
                String encodedSearch = URLEncoder.encode(searchText, "UTF-8");
                String searchUrl = "https://api.twitter.com/1.1/search/tweets.json?q=" + encodedSearch;
                new GetTweetsTask().execute(searchUrl);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void volley(View view) {
        TextView textView = findViewById(R.id.textView);
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        StringRequest request = new StringRequest(
                com.android.volley.Request.Method.GET,
                "https://jsonplaceholder.typicode.com/users",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        textView.setText("Ответ: " + response);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        textView.setText("Ошибка");
                    }
                });
        queue.add(request);
    }

    public void retrofit(View view) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MessageApi messageApi = retrofit.create(MessageApi.class);
        retrofit2.Call<List<User>> users = messageApi.getUsers();
        users.enqueue(new retrofit2.Callback<List<User>>() {
            @Override
            public void onResponse(retrofit2.Call<List<User>> call, retrofit2.Response<List<User>> response) {
                int size = response.body().size();
                Log.d("Hello", "Response body: " + size);
                for (int i = 0; i < response.body().size(); i++) {
                    Log.d("Hello", "i: " + response.body().get(i).id);
                    Log.d("Hello", "i: " + response.body().get(i).name);
                    Log.d("Hello", "i: " + response.body().get(i).username);
                    Log.d("Hello", "i: " + response.body().get(i).email);
                    Log.d("Hello", "i: " + response.body().get(i).website);
                    Log.d("Hello", "i: " + response.body().get(i).phone);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<List<User>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private class GetTweetsTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... searchUrls) {
            StringBuilder tweetFeedBuilder = new StringBuilder();
            for (String searchUrl : searchUrls) {
                OkHttpClient client = new OkHttpClient();
                try {
                    Request request = new Builder()
                            .url(searchUrl)
                            .build();
                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(@NotNull Call call, @NotNull IOException e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                            if (response.isSuccessful()) {
                                InputStream inputStream = response.body().byteStream();
                                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                                String line;
                                while ((line = bufferedReader.readLine()) != null) {
                                    tweetFeedBuilder.append(line).append("\n");
                                }
                            } else {
                                throw new IOException("Unexpected code: " + response);
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return tweetFeedBuilder.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Toast.makeText(MainActivity.this, result, Toast.LENGTH_LONG).show();
        }
    }
}