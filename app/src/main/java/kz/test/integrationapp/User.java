package kz.test.integrationapp;

public class User {

    String id;
    String name;
    String username;
    String email;
    String phone;
    String website;

    public User(
            String id,
            String name,
            String username,
            String email,
            String phone,
            String website
    ) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.website = website;
    }
}
